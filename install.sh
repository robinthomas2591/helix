#!/bin/bash

. install/config.sh
. install/util.sh

# Check for root user.
if [ "$(whoami)" != "root" ]; then
  echo "[*] You need to be root to run docker."
  exit 1
fi

# Check for Docker and Docker-compose.
DOCKER_EXISTS=$(command -v docker >/dev/null 2>&1 || echo "1")
DOCKER_COMPOSE_EXISTS=$(command -v docker-compose >/dev/null 2>&1 || echo "1")
if [ ! -z "$DOCKER_EXISTS" ]; then
  echo "[*] Docker is not installed! Please install it."
elif [ ! -z "$DOCKER_COMPOSE_EXISTS" ]; then
  echo "[*] Docker-compose is not installed! Please install it."
else
  docker-compose build
  docker-compose up -d
  #docker-compose scale app=2

  # Copy the WAR file.
  copy_war_file "helix_app_1"
  #copy_war_file "helix_app_2"
fi

