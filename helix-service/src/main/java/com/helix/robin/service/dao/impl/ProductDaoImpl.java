package com.helix.robin.service.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.helix.robin.dto.Product;
import com.helix.robin.service.dao.ProductDao;
import com.helix.robin.service.dao.ProductRepository;

@Service
public class ProductDaoImpl implements ProductDao {

	@Autowired
	private ProductRepository repository;

	@Override
	public void insertList(List<Product> dtoList) {
		if (!CollectionUtils.isEmpty(dtoList)) {
			for (Product dto : dtoList) {
				// If the timestamp of the request is before the timestamp of
				// the already saved object,
				// then ignore the request and don't save it.
				Optional<Product> product = repository.findById(dto.getId());
				if (product.isPresent()) {
					Timestamp timestamp = product.get().getTimestamp();
					if (timestamp.after(dto.getTimestamp())) {
						continue;
					}
				}

				repository.save(dto);
			}
		}
	}

	@Override
	public List<Product> searchList(List<Product> dtoList) {
		List<Product> products = new ArrayList<>();

		if (!CollectionUtils.isEmpty(dtoList)) {
			List<Long> idList = dtoList.stream().map(dto -> dto.getId())
					.collect(Collectors.toList());

			products = repository.findAllById(idList);
		}

		return products;
	}

}
