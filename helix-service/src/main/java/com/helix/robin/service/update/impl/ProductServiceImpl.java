package com.helix.robin.service.update.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.helix.robin.dto.Product;
import com.helix.robin.dto.Request;
import com.helix.robin.service.dao.ProductDao;
import com.helix.robin.service.update.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDao productDao;

	@Override
	public void add(Request request) {
		List<Product> products = request.getProducts();

		for (Product product : products) {
			product.setTimestamp(request.getTimestamp());
		}

		productDao.insertList(products);
	}

	@Override
	public List<Product> search(Request request) {
		return productDao.searchList(request.getProducts());
	}

}
