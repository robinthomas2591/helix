package com.helix.robin.service.update;

import java.util.List;

import com.helix.robin.dto.Product;
import com.helix.robin.dto.Request;

public interface ProductService {
	public void add(Request request);

	public List<Product> search(Request request);
}