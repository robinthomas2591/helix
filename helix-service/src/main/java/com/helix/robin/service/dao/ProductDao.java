package com.helix.robin.service.dao;

import java.util.List;

import com.helix.robin.dto.Product;

public interface ProductDao {
	public void insertList(List<Product> dtoList);

	public List<Product> searchList(List<Product> dtoList);
}