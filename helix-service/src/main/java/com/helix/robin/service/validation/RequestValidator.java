package com.helix.robin.service.validation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.helix.robin.dto.Request;
import com.helix.robin.service.validation.spec.ValidationErrors;

@Service
public class RequestValidator {
	public List<ValidationErrors> validateTimestamp(Request request) {
		List<ValidationErrors> errors = new ArrayList<>();

		// Check whether its a valid request.
		LocalDate requestDate = request.getTimestamp().toLocalDateTime()
				.toLocalDate();
		LocalDate now = LocalDate.now();
		if (requestDate.isAfter(now) || requestDate.isBefore(now)) {
			errors.add(ValidationErrors.INVALID_TIMESTAMP);
		}

		return errors;
	}
}