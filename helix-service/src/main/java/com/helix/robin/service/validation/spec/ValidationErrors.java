package com.helix.robin.service.validation.spec;

public enum ValidationErrors {
	NO_ERROR("NO_ERROR"),

	INVALID_TIMESTAMP(
			"Invalid request timestamp. Please check your system clock"), INVALID_PRODUCT_ID(
			"One or more products do not exists in DB");

	private String errorCode;

	ValidationErrors(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return this.errorCode;
	}
}