package com.helix.robin.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.helix.robin.dto.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
