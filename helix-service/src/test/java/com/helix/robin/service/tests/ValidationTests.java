package com.helix.robin.service.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.helix.robin.dto.Request;
import com.helix.robin.service.tests.config.ValidationTestConfig;
import com.helix.robin.service.validation.RequestValidator;
import com.helix.robin.service.validation.spec.ValidationErrors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ValidationTestConfig.class})
public class ValidationTests {

	private static final Timestamp INVALID_TIMESTAMP_1 = Timestamp
			.from(Instant.EPOCH);
	private static final Timestamp INVALID_TIMESTAMP_2 = Timestamp
			.from(Instant.MAX);
	private static final Timestamp INVALID_TIMESTAMP_3 = Timestamp
			.from(Instant.MIN);
	private static final Timestamp VALID_TIMESTAMP = Timestamp.from(Instant
			.now());

	@Autowired
	private RequestValidator validator;

	@Test
	public void smokeTest() {
		assertTrue(!Objects.isNull(validator));
	}

	@Test
	public void testValidationErrorStrings() {
		assertNotNull(ValidationErrors.INVALID_TIMESTAMP.toString());
		assertNotNull(ValidationErrors.INVALID_PRODUCT_ID.toString());
		assertNotNull(ValidationErrors.valueOf("NO_ERROR"));
	}

	@Test
	public void testInvalidTimestamp() {
		Request dto = Request.builder().timestamp(INVALID_TIMESTAMP_1).build();
		List<ValidationErrors> errorList = validator.validateTimestamp(dto);
		assertTrue(errorList.contains(ValidationErrors.INVALID_TIMESTAMP));

		dto = Request.builder().timestamp(INVALID_TIMESTAMP_2).build();
		errorList = validator.validateTimestamp(dto);
		assertTrue(errorList.contains(ValidationErrors.INVALID_TIMESTAMP));

		dto = Request.builder().timestamp(INVALID_TIMESTAMP_3).build();
		errorList = validator.validateTimestamp(dto);
		assertTrue(errorList.contains(ValidationErrors.INVALID_TIMESTAMP));

		dto = Request.builder().timestamp(VALID_TIMESTAMP).build();
		errorList = validator.validateTimestamp(dto);
		assertTrue(!errorList.contains(ValidationErrors.INVALID_TIMESTAMP));
	}

}