package com.helix.robin.service.tests;

import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.helix.robin.dto.Product;
import com.helix.robin.dto.Request;
import com.helix.robin.service.tests.config.ValidationTestConfig;
import com.helix.robin.service.update.ProductService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ValidationTestConfig.class})
public class Tests {
	@Autowired
	private ProductService service;

	@Test
	public void smokeTest() {
		assertTrue(!Objects.isNull(service));
	}

	@Test
	public void testEmptyRepo() {
		Request request = Request.builder()
				.products(Arrays.asList(Product.builder().id(1).build()))
				.build();

		List<Product> products = service.search(request);
		assertTrue(products.size() == 0);
	}

	@Test
	public void testEmptySearch() {
		Request request = Request.builder().products(new ArrayList<>()).build();

		List<Product> products = service.search(request);
		assertTrue(products.size() == 0);
	}

	@Test
	public void testInsert() {
		Timestamp timestamp = Timestamp.from(Instant.now());
		Product p1 = Product.builder().id(1).build();
		Product p2 = Product.builder().id(2).build();

		Request request = Request.builder().timestamp(timestamp)
				.products(Arrays.asList(p1, p2)).build();
		service.add(request);

		List<Product> products = service.search(request);
		assertTrue(products.size() != 0);
	}

	@Test
	public void testInvalidTimestampInsert() {
		Timestamp timestamp = Timestamp.from(Instant.now());
		Product p2 = Product.builder().id(2).build();

		Request request = Request.builder().timestamp(timestamp)
				.products(Arrays.asList(p2)).build();
		service.add(request);

		request = Request.builder().timestamp(Timestamp.from(Instant.EPOCH))
				.products(Arrays.asList(Product.builder().id(2).build()))
				.build();
		service.add(request);
	}
}
