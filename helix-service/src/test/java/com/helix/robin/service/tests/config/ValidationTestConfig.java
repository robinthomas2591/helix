package com.helix.robin.service.tests.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.helix.robin.service.dao.ProductDao;
import com.helix.robin.service.dao.ProductRepository;
import com.helix.robin.service.dao.impl.ProductDaoImpl;
import com.helix.robin.service.tests.mock.MockProductRepository;
import com.helix.robin.service.update.ProductService;
import com.helix.robin.service.update.impl.ProductServiceImpl;
import com.helix.robin.service.validation.RequestValidator;

@Configuration
public class ValidationTestConfig {

	@Bean
	public RequestValidator validator() {
		return new RequestValidator();
	}

	@Bean
	public ProductService productService() {
		return new ProductServiceImpl();
	}

	@Bean
	public ProductDao productDao() {
		return new ProductDaoImpl();
	}

	@Bean
	public ProductRepository productRepository() {
		return new MockProductRepository();
	}

}
