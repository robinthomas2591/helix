package com.helix.robin.front.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.helix.robin.dto.Product;
import com.helix.robin.dto.Request;
import com.helix.robin.front.controller.endpoints.EndPoints;
import com.helix.robin.service.update.ProductService;
import com.helix.robin.service.validation.RequestValidator;
import com.helix.robin.service.validation.spec.ValidationErrors;

@RestController
@EnableAutoConfiguration
public class ProductController {

	@Autowired
	private ProductService productService;

	@Autowired
	private RequestValidator requestValidator;

	@GetMapping(value = EndPoints.GET_PRODUCTS)
	public ResponseEntity<?> getProducts(@RequestBody Request request) {
		List<ValidationErrors> errors = requestValidator
				.validateTimestamp(request);

		List<String> errorResponseList = errors.stream()
				.map(error -> error.toString()).collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(errorResponseList)) {
			Map<String, List<String>> errorMap = new HashMap<>();
			errorMap.put("errors", errorResponseList);
			return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
		}

		// Get the product list from DB.
		List<Product> products = productService.search(request);

		// Check whether any items not in DB are requested.
		if (request.getProducts().size() != products.size()) {
			Map<String, String> errorMap = new HashMap<>();
			errorMap.put("errors",
					ValidationErrors.INVALID_PRODUCT_ID.toString());
			return new ResponseEntity<>(errorMap,
					HttpStatus.UNPROCESSABLE_ENTITY);
		}

		// Return the product list.
		return new ResponseEntity<>(products, HttpStatus.OK);
	}

	@PutMapping(value = EndPoints.PUT_PRODUCTS)
	public ResponseEntity<?> putProducts(@RequestBody Request request) {
		List<ValidationErrors> errors = requestValidator
				.validateTimestamp(request);

		List<String> errorResponseList = errors.stream()
				.map(error -> error.toString()).collect(Collectors.toList());
		if (!CollectionUtils.isEmpty(errorResponseList)) {
			Map<String, List<String>> errorMap = new HashMap<>();
			errorMap.put("errors", errorResponseList);
			return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
		}

		productService.add(request);

		return new ResponseEntity<>(HttpStatus.OK);
	}
}