package com.helix.robin.front.controller.endpoints;

public class EndPoints {
	public static final String VERSION = "v1";
	public static final String ROOT = "/api/" + VERSION + "/";

	public static final String GET_PRODUCTS = ROOT + "products";
	public static final String PUT_PRODUCTS = ROOT + "products";
}
