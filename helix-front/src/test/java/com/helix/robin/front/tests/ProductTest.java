package com.helix.robin.front.tests;

import static org.junit.Assert.assertTrue;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.helix.robin.dto.Product;
import com.helix.robin.dto.Request;
import com.helix.robin.front.controller.endpoints.EndPoints;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class ProductTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void putRequest() throws Exception {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		Request requestObj = Request
				.builder()
				.id("1")
				.products(
						Arrays.asList(Product.builder().id(1).name("Product 1")
								.quantity(1).saleAmount(1.5).build()))
				.timestamp(Timestamp.from(Instant.now())).build();
		String requestJson = ow.writeValueAsString(requestObj);

		HttpEntity<String> entity = new HttpEntity<String>(requestJson, headers);

		ResponseEntity<String> responseEntity = this.restTemplate.exchange(
				EndPoints.PUT_PRODUCTS, HttpMethod.PUT, entity, String.class);

		assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
	}

}
